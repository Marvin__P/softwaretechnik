import java.util.ArrayList;

public class Professor extends Employee {

    private ArrayList<Course> courses;
    private ArrayList<Exam> exams;

    Professor(String name, int id, Department department) {
        super(name, id, department);
    }

    public void publicCourse() {

    }

    public void assignTA() {

    }

    public void addCourse() {

    }
}