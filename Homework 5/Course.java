import java.util.ArrayList;

public class Course {
    
    private int id;
    private String name;
    private int maxCapacity;
    private boolean isFull;
    private Department department;
    private ArrayList<Professor> profs;
    private ArrayList<TA> ta;
    private ArrayList<Exam> exams;
    private ArrayList<Project> projects;

    //With given constructor only one prof will be added to each course, diagramm however shows
    //0 to 3 Professors for each course, change Professor prof into a ArrayList
    Course (int id, String name, int cap, ArrayList<Professor> profs){

    }

    public void enroll (Student student){

    }

    public void apply (TA ta){

    }

    public ArrayList<Professor> getProf(){
        return profs;
    }

    public ArrayList<TA> getTA(){
        return ta;
    }

    public void setTA(ArrayList<TA> ta){

    }

    public void setName (String name){

    }
}
