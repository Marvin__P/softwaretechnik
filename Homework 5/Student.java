import java.util.ArrayList;

public class Student {
    
    private int id;
    private String name;
    private ArrayList<Course> courses;
    private ArrayList<Exam> exams;
    private Project project;
    
    public Student (){

    }

    public String getName(){
        return name;
    }

    public Project getProject(){
        return project;
    }
}
