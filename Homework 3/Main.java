//Marvin Prang, 30.10.22

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Current Floor is 5");
        System.out.println("Enter destination Floor: (0 to 10):");
        int dest_floor = in.nextInt();
        //Only a single check for wrong input, could be extended to avoid wrong input
        if (dest_floor < 0 || dest_floor > 10){
            System.out.println("Floor not found. \nPlease insert your destination Floor again: (0 to 10):");
            dest_floor = in.nextInt();
        }
        in.close();
        System.out.println("------------------------------------------------------------------------------");
        Elevator elv=new Elevator(dest_floor);

        //Not needed for the task, just to not leave an unused object
        elv.arrive();
    }
}