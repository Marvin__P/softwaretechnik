//Marvin Prang, 30.10.22

public class Elevator {
    public static int current_floor=5;
    private Elv_States State = new Elv_States();
    private int dest_floor;

    public Elevator (int dest_floor){
        this.dest_floor = dest_floor;
        System.out.println("*** Destination floor is: " + dest_floor);
        arrive_atFloor();
    }

    //Except giving a start output this function does the same as the function for the Transition arrive_to_floor.
    //You would only need one, since the Transition was named diffrently than the function given on github i created two.
    private void arrive_atFloor(){
        State.current_state = "Start";
        System.out.println("Current State: " + State.current_state + "\nMove to " + State.Idle);
        State.current_state = State.Idle;
        System.out.println("Current State: " + State.current_state + "\nFloor: " + current_floor);
        if (current_floor == dest_floor){
            System.out.println("Transition to perform: exit \nWill terminate after this transition.");
            exit();
        } else {
            if (current_floor > dest_floor){
                System.out.println("Transition to perform: go_down \nWill go from "+ State.current_state + " to " + State.Moving_down);
                current_floor --;
                go_down();
            }
            else {
                System.out.println("Transition to perform: go_up \nWill go from "+ State.current_state + " to " + State.Moving_up);
                current_floor ++;
                go_up();
            }
        }
    }

    //For the transition arrive_to_floor
    //After transition arrive_to_floor is executed go_down and go_up would never happen, because the elevator is on the right floor.
    //Still i decided to leave it, so the code is one to one depiction of the State Machine
    private void arrive_to_floor(){
        State.current_state = State.Idle;
        System.out.println("Current State: " + State.current_state + "\nFloor: " + current_floor);
        if (current_floor == dest_floor){
            System.out.println("Transition to perform: exit \nWill terminate after this transition.");
            exit();
        } else {
            if (current_floor > dest_floor){
                System.out.println("Transition to perform: go_down \nWill go from "+ State.current_state + " to " + State.Moving_down);
                current_floor --;
                go_down();
            }
            else {
                System.out.println("Transition to perform: go_up \nWill go from "+ State.current_state + " to " + State.Moving_up);
                current_floor ++;
                go_up();
            }
        }
    }

    //For the transition go_down
    private void go_down(){
        State.current_state = State.Moving_down;
        System.out.println("Current State: " + State.current_state + "\nFloor: " + current_floor);
        if (current_floor==dest_floor){
            System.out.println("Transition to perform: arrive_to_floor \nWill go from "+ State.current_state + " to " + State.Idle);
            arrive_to_floor();
        } else {
            System.out.println("Transition to perform: go_down \nWill go from "+ State.current_state + " to " + State.Moving_down);
            current_floor --;
            go_down();
        }
    }

    //For the transition go_up
    private void go_up(){
        State.current_state = State.Moving_up;
        System.out.println("Current State: " + State.current_state + "\nFloor: " + current_floor);
        if (current_floor==dest_floor){
            System.out.println("Transition to perform: arrive_to_floor \nWill go from "+ State.current_state + " to " + State.Idle);
            arrive_to_floor();
        } else {
            System.out.println("Transition to perform: go_up \nWill go from "+ State.current_state + " to " + State.Moving_up);
            current_floor ++;
            go_up();
        }
    }

    //For the transition exit
    private void exit(){
        State.current_state = "Terminated";
        System.out.println("Current State: " + State.current_state + "\nFloor: " + current_floor);
    }

    //Good bye text
    public void arrive(){
        System.out.println("------------------------------------------------------------------------------");
        System.out.println("You arrived at your wished destination. Please leave the elevator.");
        System.out.println("------------------------------------------------------------------------------");
    }
}
